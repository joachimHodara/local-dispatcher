import { Request, Response } from 'express';
import Task, { ITask } from './../models/taskModel';

export const addTask = (req: Request, res: Response): void => {
    const task = new Task(req.body);

    task.save((err: Error) => {
        if (err) {
            res.status(500);
            res.send(`Failed to add task ${task.name}. Error: ${err}`);
        } else {
            res.json(task);
        }
    });
};

export const getTasks = (req: Request, res: Response): void => {
    Task.find((err: Error, tasks: ITask[]) => {
        if (err) {
            res.status(500);
            res.send(`Failed to load tasks. Error: ${err}`);
        } else {
            res.json(tasks);
        }
    });
};

export const getTask = (req: Request, res: Response): void => {
    Task.findById(req.params.id, (err: Error, task: ITask) => {
        if (err) {
            res.status(500);
            res.send(`Failed to load task ${req.params.id}. Error: ${err}`);
        } else {
            res.json(task);
        }
    });
};

export const deleteTask = (req: Request, res: Response): void => {
    Task.deleteOne({ _id: req.params.id }, (err: Error) => {
        if (err) {
            res.status(500);
            res.send(`Failed to delete task ${req.params.id}. Error: ${err}`);
        } else {
            res.send(`Successfully deleted task ${req.params.id}`);
        }
    });
};

export const updateTask = (req: Request, res: Response): void => {
    Task.findByIdAndUpdate(req.params.id, req.body, (err: Error, task: ITask | null) => {
        if (err) {
            res.status(500);
            res.send(`Failed to update task ${req.params.id}. Error: ${err}`);
        } else {
            res.json(task);
        }
    });
};
