import * as express from 'express';
import * as taskController from './../../controllers/taskController';

export const tasklistRouter = express.Router();

tasklistRouter.get('/', taskController.getTasks);

tasklistRouter.post('/', taskController.addTask);

tasklistRouter.delete('/:id', taskController.deleteTask);
