import * as express from 'express';
import { tasklistRouter } from './tasklist';

export const router = express.Router();

router.use('/tasklist', tasklistRouter);
