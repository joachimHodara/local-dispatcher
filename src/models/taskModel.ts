import * as mongoose from 'mongoose';

export interface ITask extends mongoose.Document {
    name: string;
    description?: string;
    priority: string;
    date: Date;
}

const TaskSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    description: String,
    priority: {
        type: String,
        enum: ['High', 'Medium', 'Low'],
        default: 'Medium',
    },
    date: {
        type: Date,
        default: Date.now,
    },
});

export default mongoose.model<ITask>('Task', TaskSchema);
